"""
Loads data
"""
import argparse
from typing import Text

from src.utils.config import load_config
from src.utils.loggers import get_logger


def data_load(config_path: Text) -> None:
    """Loads data
    Args:
        config_path(Text): path to config
    """

    config = load_config(config_path)
    logger = get_logger('DATA_LOAD', log_level=config.base.log_level)


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    data_load(args.config)
