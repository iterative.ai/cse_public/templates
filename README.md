# DVC project template (venv)

[Cookiecutter](https://github.com/cookiecutter/cookiecutter) template for DVC project with use of `venv` as virtual environment tool.


## Template structure

```
{{cookiecutter.project_slug}}/
├── data                            <------ data directory
│   ├── processed
│   └── raw
├── models                          <------ ML models
├── notebooks                       <------ Jupyter notebooks 
├── reports                         <------ reports & metrics
├── src                             <------ source code modules
│   ├── stages                      <------ DVC stage scripts
│   │   ├── data_load.py
│   │   ├── data_preprocess.py
│   │   ├── evaluate.py
│   │   └── train.py
│   └── utils                       <------ Util scripts
│       ├── config.py
│       └── loggers.py
├── vscode                          <------ VSCode workspace settings, will be renamed to .vscode after generation
│   └── settings.json
├── dvc.yaml                        <------ DVC pipeline decription file
├── params.yaml                     <------ DVC pipeline config file
├── README.md                       <------ Project readme file in markdown
└── requirements.txt                <------ Python packages list
```

## Features

- Minimal `DVC` project structure including files `dvc.yaml` and `params.yaml`
- Virtual environment initialization (minimum packages installation)
- `Git`+`DVC` repository initialization


## Generate project

Install `cookiecutter`:

```bash
pip install cookiecutter
```

Generate `DVC` project:

```bash
cookiecutter https://gitlab.com/iterative.ai/cse/rnd/dvc-project-template-venv.git
```