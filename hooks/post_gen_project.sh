#!/usr/bin/env bash

echo "Add VSCode workspace settings"
mv vscode .vscode

echo "Init Git repo"
git init

echo "Build environment"
python3 -m venv venv
echo 'export PYTHONPATH=.' >> venv/bin/activate
source venv/bin/activate
pip install --upgrade pip setuptools wheel
pip install -r requirements.txt

echo "Init DVC repo"
dvc init

echo "Commit changes"
git add .
git commit -m "Init DVC repository"

deactivate